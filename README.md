## How to provision your private environment with Terraform on SkyAtlas

Terraform is a quite powerful and skilled automation tool for infrastructure management. Subdirectories in this repo will allow you to provision a virtual private environment on [SkyAtlas Public Cloud](https://www.skyatlas.com).

This is how you install Terraform but, checking [Terraform Website](https://www.terraform.io/) for the latest way(s) of installation is still recommend.

```bash
$ wget https://releases.hashicorp.com/terraform/0.12.16/terraform_0.12.16_linux_amd64.zip
$ unzip terraform_0.12.16_linux_amd64.zip
$ sudo mv terraform /usr/local/bin/
```

For identity verification we’ll use an RC file, to get that file login [SkyAtlas dashboard](https://panel.skyatlas.com) and download your RC file.

Navigate the folder you want to deploy and fallow the README.md files in those folders for further instructions.

If you need more information check out [this article](https://medium.com/@onur.ozkan83/how-to-deploy-a-kubernetes-cluster-on-skyatlas-with-kubespray-6198a24710fa).

Enjoy.
