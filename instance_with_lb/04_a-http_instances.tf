# Create HTTP Instance with 5G volume
resource "openstack_compute_instance_v2" "http" {
  count           = var.number_of_http_instances
  name            = "http-${count.index}"
  flavor_name     = "${var.http_flavor_name}"
  key_pair        = "${openstack_compute_keypair_v2.terraform.name}"
  security_groups = ["${openstack_networking_secgroup_v2.http_sec_group.name}"]

block_device {
    uuid                  = "${var.http_image_uuid}"
    source_type           = "image"
    volume_size           = 5
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
}

network {
    uuid = "${openstack_networking_network_v2.http.id}"
  }
}
