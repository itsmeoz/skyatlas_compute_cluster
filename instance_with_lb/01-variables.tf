# Skyatlas Public Network
variable "pool" {
  default = "public_floating_net"
}

# Flavor for http instances
variable "http_flavor_name" {
  default = "General M"
}

# Flavor for db instances
variable "db_flavor_name" {
  default = "Compute L"
}

# Image for http instances: Ubuntu 18.04
variable "http_image_uuid" {
  default = "f3b32b04-5bb6-4f8a-873f-b1e0f686dbe6"
}

# Image for db instances: Ubuntu 18.04
variable "db_image_uuid" {
  default = "f3b32b04-5bb6-4f8a-873f-b1e0f686dbe6"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

variable "number_of_http_instances" {
  type    = number
  default = 3
}

variable "number_of_db_instances" {
  type    = number
  default = 3
}

# SkyAtlas RC file variables
variable "os_username" {}
variable "os_project_name" {}
variable "os_password_input" {}
variable "os_auth_url" {}
variable "os_region_name" {}
variable "ssh_key_file" {}
