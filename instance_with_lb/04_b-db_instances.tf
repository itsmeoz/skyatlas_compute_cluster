# Create DB Instance with 10G volume
resource "openstack_compute_instance_v2" "db" {
  count           = var.number_of_db_instances
  name            = "db-${count.index}"
  flavor_name     = "${var.db_flavor_name}"
  key_pair        = "${openstack_compute_keypair_v2.terraform.name}"
  security_groups = ["${openstack_networking_secgroup_v2.db_sec_group.name}"]

block_device {
    uuid                  = "${var.db_image_uuid}"
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
}

network {
    uuid = "${openstack_networking_network_v2.db.id}"
  }
}
