#### CREATE NETWORKS ###

# http network
data "openstack_networking_network_v2" "http" {
  name = "${var.pool}"
}

resource "openstack_networking_network_v2" "http" {
  name           = "http"
  admin_state_up = "true"
}

# Create Subnet
resource "openstack_networking_subnet_v2" "http" {
  name            = "http"
  network_id      = "${openstack_networking_network_v2.http.id}"
  cidr            = "10.0.0.0/24"
  ip_version      = 4
  dns_nameservers = ["8.8.8.8", "1.1.1.1"]
}

# Create Router
resource "openstack_networking_router_v2" "http" {
  name                = "http"
  admin_state_up      = "true"
  external_network_id = "${data.openstack_networking_network_v2.http.id}"
}

# Connect Network Elements
resource "openstack_networking_router_interface_v2" "http" {
  router_id = "${openstack_networking_router_v2.http.id}"
  subnet_id = "${openstack_networking_subnet_v2.http.id}"
}

# DB NETWORK
data "openstack_networking_network_v2" "db" {
  name = "${var.pool}"
}

resource "openstack_networking_network_v2" "db" {
  name           = "db"
  admin_state_up = "true"
}

# Create Subnet
resource "openstack_networking_subnet_v2" "db" {
  name            = "db"
  network_id      = "${openstack_networking_network_v2.db.id}"
  cidr            = "10.10.10.0/24"
  ip_version      = 4
  dns_nameservers = ["8.8.8.8", "1.1.1.1"]
}

# Create Router
resource "openstack_networking_router_v2" "db" {
  name                = "db"
  admin_state_up      = "true"
  external_network_id = "${data.openstack_networking_network_v2.db.id}"
}

# Connect Network Elements
resource "openstack_networking_router_interface_v2" "db" {
  router_id = "${openstack_networking_router_v2.db.id}"
  subnet_id = "${openstack_networking_subnet_v2.db.id}"
}


