# Create Security Group For http Instances
resource "openstack_networking_secgroup_v2" "http_sec_group" {
  name        = "http_sec_group"
  description = "Security group for the HTTP example instances"
}

# Allow port 22
resource "openstack_networking_secgroup_rule_v2" "http_22" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.http_sec_group.id}"
}

# Allow port 80
resource "openstack_networking_secgroup_rule_v2" "http_80" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.http_sec_group.id}"
}

# Allow icmp
resource "openstack_networking_secgroup_rule_v2" "http_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.http_sec_group.id}"
}

# Create Security Group For http Instances
resource "openstack_networking_secgroup_v2" "db_sec_group" {
  name        = "db_sec_group"
  description = "Security group for the DB example instances"
}

# Allow port 22
resource "openstack_networking_secgroup_rule_v2" "db_22" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.db_sec_group.id}"
}

# Allow port 80
resource "openstack_networking_secgroup_rule_v2" "db_3306" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.db_sec_group.id}"
}

# Allow icmp
resource "openstack_networking_secgroup_rule_v2" "db_icmp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.db_sec_group.id}"
}


# Create ssh-key
resource "openstack_compute_keypair_v2" "terraform" {
  name       = "terraform"
  public_key = "${file("${var.ssh_key_file}.pub")}"
}
