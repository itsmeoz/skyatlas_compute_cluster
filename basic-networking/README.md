# skyatlas_compute_cluster

This example creates a simple private virtual network on SkyAtlas Platform.


![Diagram](https://gitlab.com/itsmeoz/skyatlas_compute_cluster/raw/master/diagrams/Basic%20Network%20Diagram.png)
