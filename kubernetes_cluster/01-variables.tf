# Skyatlas Public Network
variable "pool" {
  default = "public_floating_net"
}

# Flavor for master nodes
variable "master_flavor_name" {
  default = "General M"
}
# Flavor for worker nodes
variable "worker_flavor_name" {
  default = "General L"
}
# Flavor for bastion nodes
variable "bastion_flavor_name" {
  default = "General M"
}
# Image for master nodes: Ubuntu 18.04
variable "master_image_uuid" {
  default = "e62b4773-ca26-4dbb-abfc-cba82ee4077d"
}
# Image for worker nodes: Ubuntu 18.04
variable "worker_image_uuid" {
  default = "e62b4773-ca26-4dbb-abfc-cba82ee4077d"
}
# Image for bastion nodes: Ubuntu 18.04
variable "bastion_image_uuid" {
  default = "e62b4773-ca26-4dbb-abfc-cba82ee4077d"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

variable "number_of_master_nodes" {
  type    = number
  default = 3
}

variable "number_of_worker_nodes" {
  type    = number
  default = 5
}

terraform {
  backend "swift" {
    container         = "terraform-state"
    archive_container = "terraform-state-archive"
  }
}
# # SkyAtlas RC file variables
# variable "os_username" {}
# variable "os_project_name" {}
# variable "os_password_input" {}
# variable "os_auth_url" {}
# variable "os_region_name" {}
# variable "ssh_key_file" {}
