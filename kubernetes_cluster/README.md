This example will allow you to provision a Kubernetes Cluster from scratch.

Start with editing the variables for your environment.
```bash
$ vim 01-variables.tf
```

Initialize working directory with provider plugins
```bash
$ terraform init
```
before you begin, source your openrc file from SkyAtlas then create an execution plan.

```bash
$ terraform plan -var os_username=$OS_USERNAME \
-var os_project_name=$OS_PROJECT_NAME \
-var os_password_input=$OS_PASSWORD_INPUT \
-var os_auth_url=$OS_AUTH_URL \
-var os_region_name=$OS_REGION_NAME \
-var ssh_key_file=demo-key
```
If you satisfied with the outputs, deploy your environment.

```bash
$ terraform apply -var os_username=$OS_USERNAME \
-var os_project_name=$OS_PROJECT_NAME \
-var os_password_input=$OS_PASSWORD_INPUT \
-var os_auth_url=$OS_AUTH_URL \
-var os_region_name=$OS_REGION_NAME \
-var ssh_key_file=demo-key
```
the output should give you the floating ip of Bastion instance.
```bash
Outputs:

bastion_fips = [
  "213.16.236.112",
]
```
![Diagram](https://gitlab.com/itsmeoz/skyatlas_compute_cluster/raw/master/diagrams/Kubernetes%20Cluster%20Diagram.png)

login to bastion
```bash
$ ssh -i demo-key 213.16.236.112 -l ubuntu
```
install ansible and pip
```bash
$ sudo apt -y update
$ sudo apt -y install ansible
$ sudo apt -y install python-pip
```
Add your private and public key on Bastion instance and set permissions.

For Kubernetes Cluster deployment we'll use Kubespray.
```bash
$ git clone https://github.com/kubernetes-sigs/kubespray.git
```
```bash
$ cd kubespray/
```
create your inventory files from example
```bash
$ cp -rfp inventory/sample inventory/mycluster
```
install requirements
```bash
$ pip install --user -r requirements.txt
```
add kubernetes nodes ip address to Bastion hosts file
```bash
$ sudo vim /etc/hosts
```
edit the inventory.ini file
```bash
$ vim inventory/mycluster/inventory.ini
```
change cloud provider configuration from group_vars/all/all.yml
```bash
cloud_provider: openstack
```
source your openrc file from SkyAtlas
```bash
$ source openrc
```
Deploy the cluster
```bash
$ ansible-playbook -i inventory/mycluster/inventory.ini --become --become-user=root cluster.yml
```
when it's done login all your Kubernetes Master nodes. With your prive key you added Bastion Node you can SSH your Kubernetes nodes.

Add the config below on /etc/kubernetes/cloud_config file

```bash
[LoadBalancer]
subnet-id=ID_OF_OPENSTACK_SUBNET_FOR_KUBERNETS_NODES
floating-network-id=ID_OF_YOUR_OPENSTACK_CLOUD_PROVIDER_FLOATING_NETWORK
```

Restart the cluster services and you good to go.

If you need more information check out [this article](https://medium.com/@onur.ozkan83/how-to-deploy-a-kubernetes-cluster-on-skyatlas-with-kubespray-6198a24710fa).
