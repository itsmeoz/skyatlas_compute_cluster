variable "pool" {
  default = "public_floating_net"
}

variable "image_uuid" {
  default = "f3b32b04-5bb6-4f8a-873f-b1e0f686dbe6"
}

variable "flavor" {
  default = "Compute S"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

variable "os_username" {}
variable "os_project_name" {}
variable "os_password_input" {}
variable "os_auth_url" {}
variable "os_region_name" {}
variable "ssh_key_file" {}


